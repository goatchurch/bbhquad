/**
 * deltatime - pru code to send eqep deltatime data to arm side program.
 * TODO: check change of direction flag? Index checking.
 * 
 * Written by Tom Clayton
 **/
 
#include <stdint.h>
#include <pru_cfg.h>
#include <sys_pwmss.h>
#include <pru_ecap.h>

// buffer //
#define BUFFER_LENGTH 	64	// Must be the same in both programs
#define	N_BUFFERS	4
//#define B_SEL_OFFSET	(BUFFER_LENGTH * 2 * 2)
#define B_SEL_OFFSET	(BUFFER_LENGTH * 2 * N_BUFFERS)
#define B_POS_OFFSET	(B_SEL_OFFSET + 1)
#define I_ERROR_OFFSET	(B_POS_OFFSET + 1)

// IO registers //
volatile register uint32_t __R30;
volatile register uint32_t __R31;

// memory mapping //
volatile near uint32_t buffer __attribute__((cregister("PRU_DMEM_0_1", near), peripheral));

// masks etc //
#define UPEVNT 	0x80	// unit position event
#define COEF 	0x08	// timer overflow
#define FIMF	0X02	// first index marker	
#define	PCE		0x02	// position counter error (interupt)
#define INT		0x01	// global interupt
#define IEL		0x400	// index interupt
#define TRUE 	1
#define FALSE 	0
#define PRU0_ARM_TRIGGER (__R31 = 3 | (1 << 5))

/* Non-CT register defines */

#define FIFO0DATA		(*((volatile unsigned int *)0x44E0D100))
#define CTRL			(*((volatile unsigned int *)0x44E0D040))
#define STEPENABLE		(*((volatile unsigned int *)0x44E0D054))
#define STEPCONFIG7		(*((volatile unsigned int *)0x44E0D094))
#define ADC_CLKDIV		(*((volatile unsigned int *)0x44E0D04C))

// functions //
void init_eqep(uint8_t pwmss);
//void init_adc();

int main(void)
{
    volatile uint32_t 	*mem_pointer = &buffer;
    
    uint32_t 	readings = 0,
    			dtime = 0;
    uint8_t		buffer_selected = 0,
				buffer_position = 0;
    uint16_t 	eqep_status,
    			flags,
    			i;

	// allow OCP master port access by the PRU so the PRU can read external memories //
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;
	// Configure GPI and GPO as Mode 0 (Direct Connect) //
	CT_CFG.GPCFG0 = 0x0000;
	// Clear GPO pins //
    __R30 = 0x0;
	
	// initialisations //
	init_eqep(0);
	init_adc();
	//while(1);
	for (i=0;i<=I_ERROR_OFFSET+1;i++)
		*(mem_pointer + i) = 0;

	//PRU0_ARM_TRIGGER;
	//__R30 = 0x1;
	//while(1);

	// main loop //
	while(1){
		buffer_selected = *(mem_pointer + B_SEL_OFFSET);
		buffer_position = *(mem_pointer + B_POS_OFFSET);
		
		if((eqep_status = PWMSS0.EQEP_QEPSTS) & UPEVNT){
			//__R30 = 0x1;
	    	// encoder pulse //
	    	dtime += PWMSS0.EQEP_QCPRD;
	    	*(mem_pointer + (buffer_selected*BUFFER_LENGTH) + buffer_position++) = dtime;
	    	*(mem_pointer + (buffer_selected*BUFFER_LENGTH) + buffer_position++) = PWMSS0.EQEP_QPOSCNT;
	    	//*(mem_pointer + (buffer_selected*BUFFER_LENGTH) + buffer_position++) = FIFO0DATA;
	    	if (buffer_position == BUFFER_LENGTH){
	    		*(mem_pointer + B_SEL_OFFSET) = buffer_selected;
	    		PRU0_ARM_TRIGGER;
	    		//buffer_selected ^= 1;
	    		buffer_selected = buffer_selected != N_BUFFERS-1 ? buffer_selected + 1 : 0;
	    		*(mem_pointer + B_SEL_OFFSET) = buffer_selected;
	    		buffer_position = 0;
	    	}
	    	PWMSS0.EQEP_QEPSTS = eqep_status | UPEVNT;
	    	dtime = 0;
		}
	    else if(eqep_status & COEF){
	    	// eqep timer overflow //
	    	dtime += 65535;
            PWMSS0.EQEP_QEPSTS = eqep_status | COEF;
	    }
	    
	    // index error checking - not in use
	    
	    //if((flags = PWMSS0.EQEP_QFLG) & PCE){
	    //	__R30 = 0x1;	    
	    //	*(mem_pointer + I_ERROR_OFFSET) = TRUE;
	    	//*(mem_pointer + I_ERROR_OFFSET+1) = PWMSS0.EQEP_QPOSILAT;
	    //	PWMSS0.EQEP_QCLR = PCE | INT;
	    //}
	    
	    // Index reporting - not in use
	    
	    //if(PWMSS0.EQEP_QFLG & IEL){
		//	//__R30 = eqep_status;
	    //	*(mem_pointer + I_ERROR_OFFSET) = TRUE;
	    //	*(mem_pointer + I_ERROR_OFFSET+1) = PWMSS0.EQEP_QPOSILAT;
	    //	PWMSS0.EQEP_QCLR = IEL | INT;
	    //}
	    
	    *(mem_pointer + B_POS_OFFSET) = buffer_position;
	    
	}
}

/*
 * Init eQEP reader
 */
void init_eqep(uint8_t pwmss) {
	switch(pwmss){
	case 0:
		// Enable PWMSS0 clock signal generation //
    	//while (!(CM_PER_EPWMSS0 & 0x2))
    	//    CM_PER_EPWMSS0 |= 0x2;
    	// Set to defaults in quadrature mode //
    	PWMSS0.EQEP_QDECCTL = 0x00;

    	// Enable EQEP Capture and set pre-scaler //
    	// 20 = CLK/4 = 25Mhz? > output 40ns//
    	// 00 = CLK = 100Mhz output 10ns //
    	PWMSS0.EQEP_QCAPCTL |= 0x8020;

    	// Clear encoder count //
		// don't need? it will be reset on first index //
    	//PWMSS1.EQEP_QPOSCNT_bit.QPOSCNT = 0x00000000;
	
    	// Set max encoder count //
    	// 2000 for a 500 line count encoder //
    	// PWMSS0.EQEP_QPOSMAX_bit.QPOSMAX = 1999;
	
		// enable position counter error interupt //
		// PWMSS0.EQEP_QEINT = IEL | PCE;
	
    	// Clear all interrupt bits //
    	PWMSS0.EQEP_QCLR = 0xFFFF;
    	break;
    case 1:
    	PWMSS1.EQEP_QDECCTL = 0x00;
    	PWMSS1.EQEP_QCAPCTL |= 0x8020;
    	PWMSS1.EQEP_QPOSMAX_bit.QPOSMAX = 1999;
    	PWMSS1.EQEP_QEINT = IEL | PCE;
    	PWMSS1.EQEP_QCLR = 0xFFFF;
    	break;
    case 2:
    	PWMSS2.EQEP_QDECCTL = 0x00;
    	PWMSS2.EQEP_QCAPCTL |= 0x8020;
    	PWMSS2.EQEP_QPOSMAX_bit.QPOSMAX = 1999;
    	PWMSS2.EQEP_QEINT = IEL | PCE;
    	PWMSS2.EQEP_QCLR = 0xFFFF;
    	break;
    default:
    	break;
	}
}

/*
 * Init analogue input, for current sense
 */
void init_adc()
{
	//set sampling rate//
	//ADC_CLKDIV = 0x00;
	
	//Enable ADC //
	CTRL |= 0x05;

	//Enable step 7 //
 	STEPENABLE |= 0x80;
	
	//Configure step 7 to continuous mode//
	STEPCONFIG7 |= 0x11;

	//	__R30 = FIFO0DATA ? 1 : 0;
}

