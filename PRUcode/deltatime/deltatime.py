
# deltatime.py - gets deltatime and current draw data
# from pru and shows line count every index pulse 
#
# writen by Tom Clayton

# capes:
# echo BB-BONE-PRU-01 > /sys/devices/bone_capemgr*/slots
# echo bone_eqep0 > /sys/devices/bone_capemgr*/slots
# echo bone_eqep1 > /sys/devices/bone_capemgr*/slots
# echo bone_eqep2 > /sys/devices/bone_capemgr*/slots
# echo cape-bone-iio > /sys/devices/bone_capemgr*/slots

import pypruss
import mmap
import struct
import sys
import time

from Adafruit_BBIO import GPIO
from Adafruit_BBIO import PWM
"""
PWMSS0_offset = 0x48300000
PWMSS0_size = 0x4830025F - PWMSS0_offset + 1
PWMSS1_offset = 0x48302000
PWMSS1_size = 0x4830225F - PWMSS1_offset + 1
PWMSS2_offset = 0x48304000
PWMSS2_size = 0x4830425F - PWMSS2_offset + 1

fp = open("/dev/mem", "r+b" )
memp0 = mmap(fp.fileno(), PWMSS0_size, offset=PWMSS0_offset)
memp1 = mmap(fp.fileno(), PWMSS1_size, offset=PWMSS1_offset)
#memp2 = mmap(fp.fileno(), PWMSS2_size, offset=PWMSS2_offset)
QPOSCNT = 0x180 + 0x00
QCAPCTL = 0x180 + 0x2C
QCPRD   = 0x180 + 0x3C
memp0[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)
memp1[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)
#memp2[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)
"""
PRU_ICSS = 0x4A300000
PRU_ICSS_LEN = 512 * 1024
BUFFER_LENGTH = 64  # Must be the same in both programs
N_BUFFERS = 4
BUFFER_LEN_BYTES = BUFFER_LENGTH * 4
#B_SEL_OFFSET = (BUFFER_LEN_BYTES * 2 * 2)
B_SEL_OFFSET = (BUFFER_LEN_BYTES * 2 * N_BUFFERS)
B_POS_OFFSET = B_SEL_OFFSET + 4
I_ERROR_OFFSET = B_POS_OFFSET + 4
BUFFER_STRING = "<" + "L" * BUFFER_LENGTH

pypruss.modprobe()  # This only has to be called once pr boot
pypruss.init()                                  
pypruss.open(0)                                
pypruss.pruintc_init() 
#pypruss.exec_program(0, "./deltatimetext.bin")           
pypruss.exec_program(0, "./text.bin") # tom's precompiled one           


#pypruss.wait_for_event(0)
#sys.exit()


with open("/dev/mem", "r+b") as f:
    pru_mem = mmap.mmap(f.fileno(), PRU_ICSS_LEN, offset=PRU_ICSS)


PWM.start("P9_16", 50, 50000, 1)
arm = "P9_12"
GPIO.setup("P9_12", GPIO.OUT)
GPIO.output("P9_12", GPIO.HIGH)

#sys.exit()

def jolt(v):
    global pru_mem
    data = []
    buffer_selected_old, buffer_position_old = struct.unpack('<LL', pru_mem[B_SEL_OFFSET: B_SEL_OFFSET + 8])
    while True:
        time.sleep(0.5)
        buffer_selected, buffer_position = struct.unpack('<LL', pru_mem[B_SEL_OFFSET: B_SEL_OFFSET + 8])
        if (buffer_selected, buffer_position) == (buffer_selected_old, buffer_position_old):
            break
        buffer_selected_old, buffer_position_old = buffer_selected, buffer_position

    pru_mem[B_SEL_OFFSET: B_SEL_OFFSET + 8] = struct.pack('<LL', 0, 0)  # overwrite the positions to the front again
    
    PWM.start("P9_16", v, 50000, 1)
    #while(1):
    
    buffer_selected_old = struct.unpack('<L', pru_mem[B_SEL_OFFSET: B_SEL_OFFSET + 4])[0]
    for i in range(4):
        
        count = 0
        
        while True:
            time.sleep(0.0001)
            buffer_selected = struct.unpack('<L', pru_mem[B_SEL_OFFSET: B_SEL_OFFSET + 4])[0]
            if buffer_selected != buffer_selected_old:
                print("advance detected from to", buffer_selected_old, buffer_selected)
                break
            count += 1
            if count == 20000:
                print("count timeout")
                PWM.start("P9_16", 50, 50000, 1)
                return data
        
        buffer_to_read, buffer_position, index, index_count  = struct.unpack('<LLLL', pru_mem[B_SEL_OFFSET: B_SEL_OFFSET + 16])
        print("buffer position", buffer_to_read, buffer_position)
        #print "ok"
        buffer_data = struct.unpack(BUFFER_STRING, 
            pru_mem[buffer_selected_old*BUFFER_LEN_BYTES: (buffer_selected_old*BUFFER_LEN_BYTES)+(BUFFER_LEN_BYTES)])
        data.extend(buffer_data)
        buffer_selected_old = buffer_to_read    
        if index:
            print "index:", index_count
            pru_mem[I_ERROR_OFFSET: I_ERROR_OFFSET+4] = struct.pack("<L", False)
            
    PWM.start("P9_16", 50, 50000, 1)
    return data

fout = open("prujolt3.txt", "w")
for v in [i*0.1  for i in range(-50, 51)]:
    data = jolt(v+50)
    print(v, len (data), data[-2:])
    time.sleep(0.4)
    fout.write("%f %s" % (v, " ".join(map(str, data))))
    fout.write("\n")
fout.close()

pypruss.pru_disable(0)
pypruss.exit() 

