/**
 * deltatimeddr - pru code to send eqep deltatime data to arm side program.
 * put ddr address and number of readings required into pru memory
 * program starts recording at next index and resets readings to zero
 * to indicate completion.
 * 
 * no error checking for number of readings + memory avail.
 * 
 * Written by Tom Clayton
 **/
 
#include <stdint.h>
#include <pru_cfg.h>
#include <sys_pwmss.h>
#include <pru_ecap.h>


// IO registers //
volatile register uint32_t __R30;
volatile register uint32_t __R31;

// memory mapping //
volatile near uint32_t pru_mem __attribute__((cregister("PRU_DMEM_0_1", near), peripheral));

// masks etc //
#define UPEVNT 	0x80	// unit position event
#define COEF 	0x08	// timer overflow
#define FIMF	0X02	// first index marker	
#define	PCE		0x02	// position counter error (interupt)
#define INT		0x01	// global interupt
#define IEL		0x400	// index interupt
#define TRUE 	1
#define FALSE 	0
#define PRU0_ARM_TRIGGER (__R31 = 3 | (1 << 5))


// functions //
void init_eqep(uint8_t pwmss);

int main(void)
{
    volatile uint32_t 	*pru_mem_ptr = &pru_mem,
    					*ddr_mem_ptr;
    uint32_t 	readings = 0,
    			dtime = 0,
    			i;
    uint16_t 	eqep_status,
    			flags;

	// allow OCP master port access by the PRU so the PRU can read external memories //
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;
	// Configure GPI and GPO as Mode 0 (Direct Connect) //
	CT_CFG.GPCFG0 = 0x0000;
	// Clear GPO pins //
    __R30 = 0x0;
	
	// initialisations //
	init_eqep(0);
	ddr_mem_ptr = *pru_mem_ptr;

	/*
	// write test 
	*ddr_mem_ptr = *(pru_mem_ptr+1);
	PRU0_ARM_TRIGGER;
	while(1);
	*/
	
	while(1){
		if(readings = *(pru_mem_ptr+1)){
			/*
			__R30 = 0x1;
			//loop test:
			for(i=0;i<readings;i++){
				*(ddr_mem_ptr+(2*i)) = i;
				*(ddr_mem_ptr+(2*i+1)) = i;
			}
			*(pru_mem_ptr + 1) = 0;
		}	*/
				
			// wait for index //
			
			PWMSS0.EQEP_QCLR = IEL | INT;
			while(!(PWMSS0.EQEP_QFLG & IEL))
				;
	    	//__R30 = 0x1;
	    	
			//PWMSS0.EQEP_QEPSTS = PWMSS0.EQEP_QEPSTS | FIMF;
			//while (!(eqep_status = PWMSS0.EQEP_QEPSTS & FIMF))
			//	;
			//__R30 = 0x0;	
			// take readings //
			
			for(i=0;i<readings;){
				if(eqep_status = PWMSS0.EQEP_QEPSTS & UPEVNT){
					__R30 = eqep_status & FIMF;			
				    // encoder pulse //
				    dtime += PWMSS0.EQEP_QCPRD;
				    *(ddr_mem_ptr+i) = dtime;
				    //*(ddr_mem_ptr+(2*i+1)) = PWMSS0.EQEP_QPOSCNT;
				    PWMSS0.EQEP_QEPSTS = eqep_status | UPEVNT;
				    dtime = 0;
				    i++;
				}	
				else if(eqep_status & COEF){
				    	
				    // eqep timer overflow //
				    dtime += 65535;
			           PWMSS0.EQEP_QEPSTS = eqep_status | COEF;
				}
			}
			
			// notify arm side all readings taken //
			*(pru_mem_ptr + 1) = 0;
		}
	}
}

/*
 * Init eQEP reader
 */
void init_eqep(uint8_t pwmss) {
	switch(pwmss){
	case 0:
		// Enable PWMSS0 clock signal generation //
    	//while (!(CM_PER_EPWMSS0 & 0x2))
    	//    CM_PER_EPWMSS0 |= 0x2;
    	// Set to defaults in quadrature mode //
    	PWMSS0.EQEP_QDECCTL = 0x00;

    	// Enable EQEP Capture and set pre-scaler //
    	// 20 = CLK/4 = 25Mhz? > output 40ns//
    	// 00 = CLK = 100Mhz output 10ns //
    	PWMSS0.EQEP_QCAPCTL |= 0x8000;

    	// Clear encoder count //
		// don't need? it will be reset on first index //
    	//PWMSS1.EQEP_QPOSCNT_bit.QPOSCNT = 0x00000000;
	
    	// Set max encoder count //
    	// 2000 for a 500 line count encoder //
    	PWMSS0.EQEP_QPOSMAX_bit.QPOSMAX = 1999;
	
		// enable position counter error interupt //
		PWMSS0.EQEP_QEINT = IEL | PCE;
	
    	// Clear all interrupt bits //
    	PWMSS0.EQEP_QCLR = 0xFFFF;
    	break;
    case 1:
    	PWMSS1.EQEP_QDECCTL = 0x00;
    	PWMSS1.EQEP_QCAPCTL |= 0x8000;
    	PWMSS1.EQEP_QPOSMAX_bit.QPOSMAX = 1999;
    	PWMSS1.EQEP_QEINT = IEL | PCE;
    	PWMSS1.EQEP_QCLR = 0xFFFF;
    	break;
    case 2:
    	PWMSS2.EQEP_QDECCTL = 0x00;
    	PWMSS2.EQEP_QCAPCTL |= 0x8000;
    	PWMSS2.EQEP_QPOSMAX_bit.QPOSMAX = 1999;
    	PWMSS2.EQEP_QEINT = IEL | PCE;
    	PWMSS2.EQEP_QCLR = 0xFFFF;
    	break;
    default:
    	break;
	}
}
