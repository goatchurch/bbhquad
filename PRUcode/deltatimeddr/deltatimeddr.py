import pypruss
import mmap
import struct
import time
import sys
from Adafruit_BBIO import PWM

pypruss.modprobe()
ddr_addr = pypruss.ddr_addr()
ddr_size = pypruss.ddr_size()

print("DDR memory address is 0x%x and the size is 0x%x" % (ddr_addr, ddr_size))

PRU_ICSS = 0x4A300000
PRU_ICSS_LEN = 512 * 1024
N_VARIABLES = 1

ddr_offset = ddr_addr - 0x10000000
ddr_filelen = ddr_size + 0x10000000
ddr_start = 0x10000000
ddr_end = 0x10000000 + ddr_size

pypruss.init()                                                      # Init the PRU
pypruss.open(0)                                                     # Open PRU event 0 which is PRU0_ARM_INTERRUPT
pypruss.pruintc_init()                                              # Init the interrupt controller
pypruss.pru_write_memory(0, 0, [ddr_addr, 0])                          # Put the ddr address in the first region
pypruss.exec_program(0, "./text.bin")                          


with open("/dev/mem", "r+b") as f:                                  
    ddr_mem = mmap.mmap(f.fileno(), ddr_filelen, offset=ddr_offset)
with open("/dev/mem", "r+b") as f:
    pru_mem = mmap.mmap(f.fileno(), PRU_ICSS_LEN, offset=PRU_ICSS)

def run(v, n_readings):
    
    PWM.start("P9_16", v, 50000, 1)
    time.sleep(5)
    
    pru_mem[4: 8] = struct.pack("L", n_readings) 
    
    count = 0
    x = struct.unpack("L", pru_mem[4: 8])[0]
    while x:
        time.sleep(1)
        x = struct.unpack("L", pru_mem[4: 8])[0]
        count += 1
        if count > 5:
            return None

    string = "L" * n_readings * N_VARIABLES
    return struct.unpack(string, ddr_mem[ddr_start:ddr_start + (n_readings*N_VARIABLES*4)])  

fout = open("multispin.txt", "w")
for v in [i*0.1  for i in range(-50, 51)]:
    data = run(v+50, 4000)
    if data:
        print(v, len (data), data[-2:])
        time.sleep(0.4)
        fout.write("%f, %s" % (v, ", ".join(map(str, data))))
        fout.write("\n")
    else:
        print(v, "timeout")
fout.close()


PWM.start("P9_16", 50, 50000, 1)

ddr_mem.close()                                                     # Close the memory
pru_mem.close()
pypruss.exit()