### What is this repository for? ###

New Beaglebone board cape design and software for Machinekit use, H-bridge controls, quadrature decoding, and other connections for the Arcitype Maghull Machine Tool.  

This time we're going to be organized and it's a top priority to make sure that the work is reproducible -- ie that someone else can check out the repository and stand a chance of following the setup instructions to get things like KiCAD and PRU compilation operating.
