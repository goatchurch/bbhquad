#!/bin/bash

# This file should copied to the startup /var/lib/cloud9 directory as a helper

echo 'Enabling the eqeps (quadrature encoder readers)'
echo bone_eqep0 > /sys/devices/bone_capemgr*/slots
echo bone_eqep1 > /sys/devices/bone_capemgr*/slots

echo 'Enabling the PWMs through cape universal
echo cape-universal > /sys/devices/bone_capemgr*/slots
config-pin P9_14 pwm
config-pin P9_16 pwm
config-pin P9_21 pwm

echo 1 > /sys/class/pwm/export
echo 3 > /sys/class/pwm/export
echo 4 > /sys/class/pwm/export


#echo 'scanning for known ESPs to help you out'
#arp-scan 192.168.0.0/24 | grep -E ':3e\s|:f5\s'

#echo 'Starting up the socket server (page will be at http://192.168.0.80/polarmotion.html)'
python3 mainpolar.py

