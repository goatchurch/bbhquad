#! /usr/local/opt/python-3.4.5/bin/python3.4

import threading, time, re, os, atexit, csv

from websocketmodule import mainasyncloop, servewebsocketrunforever, asyncqueueoutward, asyncqueueinward
from websocketmodule import setstartpositionstring

from motorcontrolmodule import arm, pwm0, pwm1, memp0, memp1, PolarPos
qdistcutoff = 4000

polarpos = PolarPos(arm, [(memp0, pwm0), (memp1, pwm1)])

atexit.register(polarpos.settostationary)

fout = open("jolt.csv", "w")

def recentre(wssendcount):
    polarpos.polaraxes[0].qstart = polarpos.polaraxes[0].qpos
    polarpos.polaraxes[1].qstart = polarpos.polaraxes[1].qpos
    polax = (polarpos.polaraxes[0].qstart, polarpos.polaraxes[1].qstart, int(wssendcount), qdistcutoff)
    setstartpositionstring("start %d %d %d %d" % polax)


#help(asyncqueueinward)
def motiondutyfunction():
    wssendcount = "0"
    prevpolax = (0, 0, 0)
    recentre(wssendcount) 

    tS = time.time()
    while True:
        bnewconnection = False
        while asyncqueueinward.qsize() > 0:
            svall, webconnectionnumber, i, n = asyncqueueinward.get_nowait()
            #print(svall, webconnectionnumber, i, n)
            lsvall = svall.split()
            comm = lsvall[0]
            if comm == "pt":
                u, v, wssendcount = lsvall[1:]
                polarpos.polaraxes[0].qdestination = int(u)
                polarpos.polaraxes[1].qdestination = int(v)
                #print(asyncqueueinward.qsize(), "goto", [pa.qdestination for pa in polarpos.polaraxes])
            elif comm == "jolt":
                res = polarpos.polaraxes[int(lsvall[1])].jolt(float(lsvall[2]), float(lsvall[3])) 
                polarpos.t0 = time.time()   # avoid big timestep because of delay by jolt
                fout.write("%d %f  " % (int(lsvall[1]), float(lsvall[2])))
                fout.write(res)
                fout.write("\n"); 
                fout.flush()
                mainasyncloop.call_soon_threadsafe(asyncqueueoutward.put_nowait, res)
            elif comm == "orbit":
                if len(lsvall) <= 4:
                    res = polarpos.realorbit(lsvall[1:]) 
                else:
                    res = polarpos.orbit(lsvall[1:]) 
                polarpos.t0 = time.time()   # avoid big timestep because of delay by jolt
                fout.write("%d  " % (float(lsvall[1])))
                fout.write(res)
                fout.write("\n"); 
                fout.flush()
                mainasyncloop.call_soon_threadsafe(asyncqueueoutward.put_nowait, res)
            elif comm == "recentre":
                recentre(wssendcount)
                bnewconnection = True
            elif comm == "newconnection":
                bnewconnection = True
            else:
                print("kkk", [svall])
        polarpos.readeqeps()
        polax = (polarpos.polaraxes[0].qpos, polarpos.polaraxes[1].qpos, int(wssendcount))
        if polax != prevpolax or bnewconnection:
            rval = "cpt %d %d %d" % polax
            rval = "%s %d" % (rval, qdistcutoff)
            mainasyncloop.call_soon_threadsafe(asyncqueueoutward.put_nowait, rval)
            if bnewconnection:
                rvald = "dest %d %d" % (polarpos.polaraxes[0].qdestination, polarpos.polaraxes[1].qdestination)
                mainasyncloop.call_soon_threadsafe(asyncqueueoutward.put_nowait, rvald)
            prevpolax = polax
        maxdist = max(abs(pa.qpos-pa.qstart)  for pa in polarpos.polaraxes)
        if maxdist > qdistcutoff:
            print("too far shutting down")
            print([pa.qpos  for pa in polarpos.polaraxes], [pa.qstart  for pa in polarpos.polaraxes])
            rval = "stop %d" % maxdist
            mainasyncloop.call_soon_threadsafe(asyncqueueoutward.put_nowait, rval)
            polarpos.settostationary()
            time.sleep(2)
            os._exit(1)
        
        polarpos.updateduty()
        ltS = time.time()
        if ltS - tS > 15:
            tS = ltS
            print([pa.qpos for pa in polarpos.polaraxes], [pa.qdestination for pa in polarpos.polaraxes], ["%.3f"%pa.qvolts for pa in polarpos.polaraxes], ["%.3f"%pa.qsumerror for pa in polarpos.polaraxes], ["%.3f"%pa.dc for pa in polarpos.polaraxes])
        time.sleep(0.01)


motiondutythread = threading.Thread(target=motiondutyfunction)
motiondutythread.setDaemon(True)
motiondutythread.start()    

servewebsocketrunforever(5678)


