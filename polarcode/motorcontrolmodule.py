#!/usr/bin/env python3

from mmap import mmap
import time, struct, sys, re, atexit, math, random
#from Adafruit_BBIO import PWM
from Adafruit_BBIO import GPIO

# The adafruit one comes up with cape-error like https://github.com/adafruit/adafruit-beaglebone-io-python/issues/114
# we would like to control/access it directly through mmap like the eqeps, but 
# there is an issue of saving 16bits through it rather than 32bit words
# for example https://graycat.io/tutorials/beaglebone-io-using-python-mmap/
# which links to https://github.com/graycatlabs/PyBBIO/blob/master/bbio/platform/beaglebone/pwm.py which does it through files
# we might need to investigate this and release a lightweight separate file that has these controls

# disable the PWM commands which lead to cape error
class CCPWM:
    pwm_number = {'P9_21': '1',
                  'P9_14': '3',
                  'P9_16': '4'}
    def start(self, channel, duty, freq, polarity):
       # with open('/sys/class/pwm/export', 'w') as f:
       #     print(self.pwm_number[channel])
       #     f.write(self.pwm_number[channel])
        self.set_duty_cycle(channel, 0)
        filename = '/sys/class/pwm/pwm' + self.pwm_number[channel]
        with open(filename + '/period_ns', 'w') as f:
            f.write('10000')
        self.set_duty_cycle(channel, 50)
        with open(filename + '/run', 'w') as f:
            f.write('1')	
    def set_duty_cycle(self, channel, duty):
        with open('/sys/class/pwm/pwm' + self.pwm_number[channel] + '/duty_ns',
                  'w') as f:
            f.write(str(int(duty/100 * 10000))) 
PWM = CCPWM()

# must config and export pwm pins before running:

# config-pin P9_14 pwm
# config-pin P9_16 pwm
# config-pin P9_21 pwm

# echo 1 > /sys/class/pwm/export
# echo 3 > /sys/class/pwm/export
# echo 4 > /sys/class/pwm/export
 

# load the capes for the eqeps using:
# echo bone_eqep0 > /sys/devices/bone_capemgr*/slots
# echo bone_eqep1 > /sys/devices/bone_capemgr*/slots

arm = "P9_12"
pwm0 = "P9_16"
pwm1 = "P9_21"
pwm2 = "P9_14"
maxvoltagepercentfrom50 = 10
maxsmoothsteppersecond = 900

PWMSS0_offset = 0x48300000
PWMSS0_size = 0x4830025F - PWMSS0_offset + 1
PWMSS1_offset = 0x48302000
PWMSS1_size = 0x4830225F - PWMSS1_offset + 1
PWMSS2_offset = 0x48304000
PWMSS2_size = 0x4830425F - PWMSS2_offset + 1

fp = open("/dev/mem", "r+b" )
memp0 = mmap(fp.fileno(), PWMSS0_size, offset=PWMSS0_offset)
memp1 = mmap(fp.fileno(), PWMSS1_size, offset=PWMSS1_offset)
#memp2 = mmap(fp.fileno(), PWMSS2_size, offset=PWMSS2_offset)
QPOSCNT = 0x180 + 0x00
QCAPCTL = 0x180 + 0x2C
QCPRD   = 0x180 + 0x3C
memp0[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)
memp1[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)
#memp2[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)

T0 = time.time()

class PolarAxis:
    def __init__(self, memp, pwm):
        self.memp = memp
        self.pwm = pwm
        self.qpos = 0
        self.qstart = None
        self.qdestination = None
        self.qsmoothdestination = None
        self.qsumerror = 0
        self.qvolts = 0
        self.dc = 50
        
    def readeqep(self):
        self.qpos = struct.unpack("<l", self.memp[QPOSCNT:QPOSCNT+4])[0]

        # fake eqep data
        #t = int((time.time() - T0)*10)/10
        #self.qpos = 20 + int(360*(math.sin(t) if self.pwm == pwm0 else math.cos(t)))

        if self.qstart is None:
            self.qstart = self.qpos
            self.qdestination = self.qpos
            self.qsmoothdestination = self.qdestination
        return self.qpos

    def updatevolts(self, dt):
        if self.qdestination is None:
            self.qvolts = 0
            return
        
        # produce a smoothly moving continuous target to aim for more tightly
        maxsmoothstep = dt * maxsmoothsteppersecond
        if abs(self.qsmoothdestination - self.qdestination) < maxsmoothstep:
            self.qsmoothdestination = self.qdestination
        else:
            self.qsmoothdestination += (maxsmoothstep if self.qsmoothdestination < self.qdestination else -maxsmoothstep)
            
        d = self.qsmoothdestination - self.qpos
        self.qsumerror += d*dt
        self.qsumerror = max(-500, min(500, self.qsumerror))
        self.qvolts = -d*0.05 - self.qsumerror*0.1   # This is your PI no-D measure
        return

    def updateduty(self, t1):
        dc = max(-maxvoltagepercentfrom50, min(maxvoltagepercentfrom50, self.qvolts))
        if self.pwm == pwm0:
            dc = -dc
        self.dc = dc+50
        PWM.set_duty_cycle(self.pwm, self.dc)

    def jolt(self, volts, duration):
        res = [ 'jolted' ]
        t0 = time.time()
        qpos0 = self.qpos
        PWM.set_duty_cycle(self.pwm, volts+50)
        while abs(self.qpos - qpos0) < 4000:
            self.readeqep()
            t = time.time() - t0
            res.append(str(int(t*1000000)))
            res.append(str(self.qpos))
            if t > duration:
                break
        PWM.set_duty_cycle(self.pwm, 50)
        while abs(self.qpos - qpos0) < 4000:
            self.readeqep()
            t = time.time() - t0
            res.append(str(int(t*1000000)))
            res.append(str(self.qpos))
            if t > duration+0.5:
                break
        self.qsmoothdestination = self.qpos
        self.qsumerror = 0
        return " ".join(res)

def acc1tovolts1(acc1, vel1):
    cv0, cm0, cv1, cm1, cvm, cvc = 53.202, -1036.634, 56.027, -974.375, -7.497, -1521.571  # polar case
    cv0, cm0, cv1, cm1, cvm, cvc = 48.979, -1267.168, 51.356, -1328.962, -16.391, 269.977  # unloaded case
    cv0, cm0, cv1, cm1, cvm, cvc = 43.324, -1046.668, 46.794, -1099.890, -7.226, 233.306   # new green condola
    
    #acc1 = vel1 - min(cdc-cv0, 0)*cm0 + numpy.maximum(cdc-cv1, 0)*cm1))*cvm + cvc
    tq = vel1 - (acc1 - cvc)/cvm
    if tq < 0:
        dv1 = tq/cm1 + cv1
    else:
        dv1 = +tq/cm0 + cv0
    return dv1

shuffuniform = []
class PolarPos:
    def __init__(self, arm, axes):
        self.arm = arm
        self.polaraxes = [ PolarAxis(memp, pwm)  for memp, pwm in axes ]
        self.updatedutycount = 0
        self.t0 = time.time()
        
        self.settostationary()
        self.readeqeps()
        self.prevprd = 0
        self.prevqpos = 0

        if self.arm:
            GPIO.output(self.arm, GPIO.HIGH)

    def settostationary(self):
        if self.arm:
            GPIO.setup(self.arm, GPIO.OUT)
            GPIO.output(arm, GPIO.LOW)
        for pa in self.polaraxes:
            if pa.pwm:
                print("set stationary", pa.pwm, self.arm)
                PWM.start(pa.pwm, 50, 100000, 1)
    
    def readeqeps(self):    
        for pa in self.polaraxes:
            pa.readeqep()

    def updateduty(self):
        self.updatedutycount += 1
        t1 = time.time()
        dt = t1 - self.t0
        self.t0 = t1
        for pa in self.polaraxes:
            pa.updatevolts(dt)
            pa.updateduty(t1)

        prd = struct.unpack("<L", self.polaraxes[0].memp[QCPRD:QCPRD+4])[0]&0xFFFF
        if prd != self.prevprd:
            kr = (self.polaraxes[0].qpos - self.prevqpos)/(dt)*prd
            #print(" ", prd, self.polaraxes[0].qpos, self.prevqpos, self.polaraxes[0].qvolts, dt, kr)
            self.prevprd = prd
            self.prevqpos = self.polaraxes[0].qpos

    def orbit(self, svals):
        pa0, pa1 = self.polaraxes
        q0c, q1c = pa0.readeqep(), pa1.readeqep()
        dcmid0, dcmid1 = pa0.dc, pa1.dc
        totaltime = float(svals[0])
        r = float(svals[1])
        vr = float(svals[2])
        tstep = float(svals[3])
        vvoltschangepersec = float(svals[4])
        axis = int(svals[5])
        qc = (q0c, q1c)[axis]
        qilo, qihi = qc - r, qc + r
        fout = open("orbit.csv", "w")
        print("writing orbit.csv")
        fout.write("t,q0,q1,v0,v1,n\n")
        t0 = time.time()   # all times relative to this point
        res = [ 'orbited' ]
        i = 0
        j = 0
        n = 0
        dcmid = (dcmid0, dcmid1)[axis]
        qic = (q0c, q1c)[axis]
        nextt = 0
        tvontsconst = 0
        vval = dcmid
        cdc = vval
        while True:
            t = time.time() - t0
            q0, q1 = pa0.readeqep(), pa1.readeqep()
            qi = (q0, q1)[axis]
            
            # outside motion window or outside time window
            if t >= nextt or (qi < qilo and vval > dcmid) or (qi > qihi and vval < dcmid): 
                if not shuffuniform:   # force the samples to be evenly distributed
                    shuffuniform.extend([x/200  for x in range(201)])
                    random.shuffle(shuffuniform)
                ulo, uhi = dcmid - (0 if qi > qic + r/2 else vr), dcmid + (0 if qi < qic - r/2 else vr)
                vval = ulo + (uhi-ulo)*shuffuniform.pop()
                #vval = random.uniform(ulo, uhi)
                nextt = t + tstep
                vvoltschangepersec = abs(vvoltschangepersec) if (vval > cdc) else -abs(vvoltschangepersec)
                tvontsconst = t + (vval - cdc)/vvoltschangepersec
                assert tvontsconst >= t
                n += 1
                print("vvalsets", nextt, vval, qi - qic, n)
            if t >= totaltime:
                break
            
            # ease the transition linearly from one voltage to the next linearly
            if axis == 0:
                cdc0 = vval if t > tvontsconst else vval - (tvontsconst - t)*vvoltschangepersec
                cdc1 = pa1.dc
            else:
                cdc1 = vval if t > tvontsconst else vval - (tvontsconst - t)*vvoltschangepersec
                cdc0 = pa0.dc
            
            PWM.set_duty_cycle(pa1.pwm, cdc1)
            PWM.set_duty_cycle(pa0.pwm, cdc0)
            fout.write("%.6f,%d,%d,%f,%f,%d\n" % (t, q0, q1, cdc0, cdc1, n))
            fout.flush()
            if abs(q0c - q0) > 2900 or abs(q1c - q1) > 2900:
                print("too far 900!!!")
                break
            i += 1
            if (len(res) < 20000) and (i%100)==1:
                res.append(str(int((t)*1000000)))
                res.append(str(pa0.qpos))
                res.append(str(pa1.qpos))

        pa0.qsmoothdestination = pa0.qpos
        #pa0.qsumerror = 0
        PWM.set_duty_cycle(pa0.pwm, dcmid0)
        pa1.qsmoothdestination = pa1.qpos
        #pa1.qsumerror = 0
        PWM.set_duty_cycle(pa1.pwm, dcmid1)
        fout.close()
        return " ".join(res)

    def realorbit(self, svals):
        #acc1 = vel1 - min(cdc-cv0, 0)*cm0 + numpy.maximum(cdc-cv1, 0)*cm1))*cvm + cvc

        pa0, pa1 = self.polaraxes
        q0c, q1c = pa0.readeqep(), pa1.readeqep()
        dcmid0, dcmid1 = pa0.dc, pa1.dc
        totaltime = float(svals[0])
        acc1fac = float(svals[1])
        cenoffset = float(svals[2])
        axis = 1
        i = 0
        fout = open("orbit2.csv", "w")
        print("writing orbit2.csv")
        fout.write("t,q0,q1,v0,v1,Bvel0,Bvel1,Bacc1\n")
        t0 = time.time()
        res = [ 'orbited' ]
        q0p, q1p = q0c, q1c
        q0p2, q1p2 = q0c, q1c
        tp, tp2 = -1, -2
        while True:
            t = time.time() - t0
            if t > totaltime:
                break
            q0, q1 = pa0.readeqep(), pa1.readeqep()
            if t - tp > 0.1:
                q0p2, q1p2 = q0p, q1p
                q0p, q1p = q0, q1
                tp2 = tp
                tp = t
            vel0 = (q0 - q0p2)/(t - tp2)
            vel1 = (q1 - q1p2)/(t - tp2)
            
            acc1 = acc1fac*(q1 - q1c - cenoffset)
            cdc0 = pa0.dc
            cdc1 = acc1tovolts1(acc1, vel1)

            fout.write("%.6f,%d,%d,%f,%f,%f,%f,%f\n" % (t, q0, q1, cdc0, cdc1, vel0, vel1, acc1))
            PWM.set_duty_cycle(pa1.pwm, cdc1)
            PWM.set_duty_cycle(pa0.pwm, cdc0)
            if abs(q0c - q0) > 900 or abs(q1c - q1) > 900:
                print("too far 900!!!")
                break
            i += 1
            if (len(res) < 20000) and (i%100)==1:
                res.append(str(int((t)*1000000)))
                res.append(str(pa0.qpos))
                res.append(str(pa1.qpos))

        pa0.qsmoothdestination = pa0.qpos
        #pa0.qsumerror = 0
        PWM.set_duty_cycle(pa0.pwm, dcmid0)
        pa1.qsmoothdestination = pa1.qpos
        #pa1.qsumerror = 0
        PWM.set_duty_cycle(pa1.pwm, dcmid1)
        fout.close()
        return " ".join(res)
        
        
