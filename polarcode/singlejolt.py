#!/usr/bin/env python3

from mmap import mmap
import time, struct, sys, re, atexit, math
from Adafruit_BBIO import PWM
from Adafruit_BBIO import GPIO


# load the capes for the eqeps using:
# echo bone_eqep0 > /sys/devices/bone_capemgr*/slots
# echo bone_eqep1 > /sys/devices/bone_capemgr*/slots

arm = "P9_12"
pwm0 = "P9_16"
pwm1 = "P9_21"
pwm2 = "P9_14"
maxvoltagepercentfrom50 = 10
maxsmoothsteppersecond = 900

PWMSS0_offset = 0x48300000
PWMSS0_size = 0x4830025F - PWMSS0_offset + 1
PWMSS1_offset = 0x48302000
PWMSS1_size = 0x4830225F - PWMSS1_offset + 1
PWMSS2_offset = 0x48304000
PWMSS2_size = 0x4830425F - PWMSS2_offset + 1

fp = open("/dev/mem", "r+b" )
memp0 = mmap(fp.fileno(), PWMSS0_size, offset=PWMSS0_offset)
memp1 = mmap(fp.fileno(), PWMSS1_size, offset=PWMSS1_offset)
#memp2 = mmap(fp.fileno(), PWMSS2_size, offset=PWMSS2_offset)
QPOSCNT = 0x180 + 0x00
QCAPCTL = 0x180 + 0x2C
QCPRD   = 0x180 + 0x3C
memp0[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)
memp1[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)
#memp2[QCAPCTL:QCAPCTL+4] = struct.pack("<L", 0x8000)

def readeqep():
    return struct.unpack("<l", memp0[QPOSCNT:QPOSCNT+4])[0]

pwm = pwm0
PWM.start(pwm, 50, 100000, 1)

def jolt(volts, duration):
    PWM.set_duty_cycle(pwm, 50)
    qpos0 = readeqep()
    while True:
        time.sleep(0.5)
        qpos = readeqep()
        if qpos == qpos0:
            break
        qpos0 = qpos
        
    t0 = time.time()
    qpos0 = readeqep()
    res = [ ]
    PWM.set_duty_cycle(pwm, volts+50)
    while True:
        qpos = readeqep()
        t = time.time() - t0
        res.append(int(t*1000000))
        res.append(qpos)
        if t > duration:
            break
        #time.sleep(0.01)
    PWM.set_duty_cycle(pwm, 50)
    return res



fout = open("jolt.txt", "w")
for v in [i*0.1 for i in range(-50, 51)]:
    jlt = jolt(v, 0.05)
    print(v, jlt[-1] - jlt[1])
    fout.write("%f %s\n" % (v, " ".join(map(str, jlt))))
fout.close()
