// SVG text generation functions, (copied from performance polar graph code)
//
var cchars = [ ]; 
cchars["A"] = [ 0, 8, 0, 2,  0, 2, 2, 0,  2, 0, 5, 0,  5, 0, 7, 2,  7, 2, 7, 8,  0, 4, 7, 4 ];

var characters = [];


characters["a"] = { "c": "a", "d": "M4 3 L3 4 L1 4 L0 3 L0 1 L1 0 L3 0 L4 1 M4 4 L4 0" };
characters["b"] = { "c": "b", "d": "M0 7 L0 0 M0 1 L1 0 L3 0 L4 1 L4 3 L3 4 L1 4 L0 3" };
characters["c"] = { "c": "c", "d": "M4 3 L 3 4 L1 4 L0 3 L0 1 L1 0 L3 0 L4 1" };
characters["d"] = { "c": "d", "d": "M4 3 L3 4 L1 4 L0 3 L0 1 L1 0 L3 0 L4 1 M4 7 L4 0" };
characters["e"] = { "c": "e", "d": "M0 2 L4 2 L4 3 L3 4 L1 4 L0 3 L0 1 L1 0 L3 0 L4 1" };
characters["f"] = { "c": "f", "d": "M1 0 L1 6 L2 7 L3 6 M0 4 L3 4" };
characters["g"] = { "c": "g", "d": "M4 3 L3 4 L1 4 L0 3 L0 1 L1 0 L3 0 L4 1 M4 4 L4 -1 L3 -2 L1 -2 L0 -1 L0 0" };
characters["h"] = { "c": "h", "d": "M0 0 L0 7 M0 3 L1 4 L3 4 L4 3 L4 0" };
characters["i"] = { "c": "i", "d": "M0 0 L0 4 M0 5 L1 6" };
characters["j"] = { "c": "j", "d": "M0 -1 L1 -2 L2 -1 L2 4 M2 5 L3 6" };
characters["k"] = { "c": "k", "d": "M0 0 L0 7 M0 3L3 0 M0 3 L2 5" };
characters["l"] = { "c": "l", "d": "M0 0 L0 7" };
characters["m"] = { "c": "m", "d": "M0 4 L0 0  M0 3  L1 4  L2 4 L3 3 L3 0  M3 3L4 4 L5 4 L6 3 L6 0" };
characters["n"] = { "c": "n", "d": "M0 4 L0 0  M0 3  L1 4  L2 4 L3 3 L3 0" };
characters["o"] = { "c": "o", "d": "M0 1 L0 3 L1 4 L3 4 L4 3 L4 1 L3 0 L1 0 L0 1" };
characters["p"] = { "c": "p", "d": "M0 -2 L0 4 M0 3 L1 4 L3 4 L4 3 L4 1 L3 0 L1 0 L0 1" };
characters["q"] = { "c": "q", "d": "M4 1 L3 0 L1 0 L0 1 L0 3 L1 4 L3 4 L4 3 M4 4 L4 -2" };
characters["r"] = { "c": "r", "d": "M0 0 L0 4 M0 3 L1 4 L2 4 L3 3" };
characters["s"] = { "c": "s", "d": "M0 1 L1 0 L2 0 L3 1 L2 2 L1 2 L0 3 L1 4 L2 4 L3 3" };
characters["t"] = { "c": "t", "d": "M1 0 L1 7 M0 4 L2 4" };
characters["u"] = { "c": "u", "d": "M0 4 L0 1 L1 0 L2 0 L3 1 L3 4" };
characters["v"] = { "c": "v", "d": "M0 4 L0 2 L2 0 L4 2 L4 4" };
characters["w"] = { "c": "w", "d": "M0 4 L0 1 L1 0 L2 0 L3 1 L3 4 M3 1 L4 0 L5 0 L6 1 L6 4" };
characters["x"] = { "c": "x", "d": "M0 4 L4 0 M0 0 L4 4" };
characters["y"] = { "c": "y", "d": "M0 4 L0 1 L1 0 L2 0 L3 1 M3 4 L3 -1 L2 -2 L1 -2 L0 0" };
characters["z"] = { "c": "z", "d": "M0 4 L4 4 L0 0 L4 0" };

characters[" "] = { "c": " ", "d": "" };
characters["1"] = { "c": "1", "d": "M2 0 L2 7 L0 5" };
characters["2"] = { "c": "2", "d": "M0 5 L0 6 L1 7 L4 7 L5 6 L5 5 L0 0 L5 0" };
characters["3"] = { "c": "3", "d": "M0 5 L0 6 L1 7 L3 7 L4 6 L4 5 L3 4 L2 4 M3 4 L4 3 L4 1 L3 0 L1 0 L0 1 L0 2" };
characters["4"] = { "c": "4", "d": "M5 3 L0 3 L4 7 L4 0" };
characters["5"] = { "c": "5", "d": "M4 7 L0 7 L0 4 L3 4 L4 3 L4 1 L3 0 L1 0 L0 1 L0 2" };
characters["6"] = { "c": "6", "d": "M4 7 L0 3 L0 1 L1 0 L3 0 L4 1 L4 3 L3 4 L1 4" };
characters["7"] = { "c": "7", "d": "M0 7 L4 7 L4 4 L0 0" };
characters["8"] = { "c": "8", "d": "M1 4 L0 3 L0 1 L1 0 L3 0 L4 1 L4 3 L3 4 L4 5 L4 6 L3 7 L1 7 L0 6 L0 5 L1 4 L3 4" };
characters["9"] = { "c": "9", "d": "M0 0 L4 4 L4 6 L3 7 L1 7 L0 6 L0 4 L1 3 L3 3" };
characters["0"] = { "c": "0", "d": "M0 2 L0 5 L2 7 L3 7 L5 5 L5 2 L3 0 L2 0 L0 2" };
characters["!"] = { "c": "!", "d": "M0 0 L0 1 M0 2 L0 7" };
characters["'"] = { "c": "'", "d": "M0 5 L1 6 L1 7" };
characters["£"] = { "c": "£", "d": "M5 5 L3 7 L2 7 L0 5 L0 0 L5 0 M-1 4 L3 4" };
characters["$"] = { "c": "$", "d": "M0 2 L0 1 L1 0 L3 0 L4 1 L4 3 L3 4 L1 4 L0 5 L0 6 L1 7 L3 7 L4 6 L4 5" };
characters["?"] = { "c": "?", "d": "M1 0 L1 1 M1 2 L1 4 L3 4 L4 5 L4 6 L3 7 L1 7 L0 6 L0 5" };

characters["A"] = { "c": "A", "d": "M0 0 L0 5 L2 7 L4 5 L4 0 M0 3 L4 3" };
characters["B"] = { "c": "B", "d": "M0 0 L0 7 L3 7 L4 6 L4 5 L3 4 L0 4 M3 4 L4 3 L4 1 L3 0 L0 0" };
characters["C"] = { "c": "C", "d": "M6 2 L4 0 L2 0 L0 2 L0 5 L2 7 L4 7 L6 5" };
characters["D"] = { "c": "D", "d": "M0 0 L0 7 L3 7 L5 5 L5 2 L3 0 L0 0" };
characters["E"] = { "c": "E", "d": "M4 0 L0 0 L0 7 L4 7 M0 4 L4 4" };
characters["F"] = { "c": "F", "d": "M0 0 L0 7 L4 7 M0 4 L4 4" };
characters["G"] = { "c": "G", "d": "M4 3 L7 3 L7 2 L5 0 L2 0 L0 2 L0 5 L2 7 L5 7 L7 5" };
characters["H"] = { "c": "H", "d": "M0 0 L0 7 M3 0 L3 7 M0 4 L3 4" };
characters["I"] = { "c": "I", "d": "M0 0 L0 7" };
characters["J"] = { "c": "J", "d": "M0 1 L1 0 L2 0 L3 1 L3 7" };
characters["K"] = { "c": "K", "d": "M0 0 L0 7 M4 0 L0 4 L4 7" };
characters["L"] = { "c": "L", "d": "M0 7 L0 0 L3 0" };
characters["M"] = { "c": "M", "d": "M0 0 L0 7 L3 4 L6 7 L6 0" };
characters["N"] = { "c": "N", "d": "M0 0 L0 7 L5 2 M5 0 L5 7" };
characters["O"] = { "c": "O", "d": "M2 0 L0 2 L0 5 L2 7 L5 7 L7 5 L7 2 L5 0 L2 0" };
characters["P"] = { "c": "P", "d": "M0 0 L0 7 L3 7 L4 6 L4 5 L3 4 L0 4" };
characters["Q"] = { "c": "Q", "d": "M2 0 L0 2 L0 5 L2 7 L5 7 L7 5 L7 2 L5 0 L2 0 M4 3 L7 0" };
characters["R"] = { "c": "R", "d": "M0 0 L0 7 L3 7 L4 6 L4 5 L3 4 L0 4 L4 0" };
characters["S"] = { "c": "S", "d": "M0 2 L0 1 L1 0 L3 0 L4 1 L4 3 L3 4 L1 4 L0 5 L0 6 L1 7 L3 7 L4 6 L4 6"  };
characters["T"] = { "c": "T", "d": "M2 0 L2 7 M0 7 L4 7"  };
characters["U"] = { "c": "U", "d": "M0 7 L0 1 L1 0 L3 0 L4 1 L4 7" };
characters["V"] = { "c": "V", "d": "M0 7 L0 2 L2 0 L4 2 L4 7" };
characters["W"] = { "c": "W", "d": "M0 7 L0 2 L2 0 L4 2 L4 7 M4 2 L6 0 L8 2 L8 7" };
characters["X"] = { "c": "X", "d": "M0 7 L0 6 L5 1 L5 0 M0 0 L0 1 L5 6 L5 7" };
characters["Y"] = { "c": "Y", "d": "M0 7 L3 4 L3 0 M3 4 L6 7" };
characters["Z"] = { "c": "Z", "d": "M0 7 L5 7 L5 6 L0 1 L0 0 L5 0" };

function convertchartopointlist(c) 
{
    var d = characters[c].d; 
    var w = characters[c].w; 
    var dtrans = Raphael.mapPath(d, Raphael.matrix()); 
    ptsl = [ ]; 
    var i = 0; 
    while (i < dtrans.length) {
        var dtransl = [ dtrans[i] ]; 
        while (true) {
            i++; 
            if ((i == dtrans.length) || (dtrans[i][0] == 'M'))
                break; 
            dtransl.push(dtrans[i]); 
        }
        ptsl.push(PolySorting.flattenpath(dtransl, cosangdot, 0.1)); 
    }
    return ptsl; 
}


function svgizetext(texttosvgize, scale)
{
    // This reduces the size of the font (the height of which is 810 units)
    if (scale == undefined)
        scale = 0.1; 
        
    var characterData; // The data for a specific character above

    // Not all characters are included. If they are missing use a specific character to indicate this.
    // For now, use a question mark.
    var missingCharacterData = characters['8'];

    var paths = []; // All of the paths
    var path = "";
    var character = '?';
    var width;
    var offsetX = 0;
    var scaledOffsetX;
    var scaledOffsetY;
    var fixedLeftOffset = 0;
    var fixedTopOffset = 200;

    var fontHeight = 810; // Don't use this but might come in handy.

var lscale = scale; 
    for (var n = 0, len = texttosvgize.length; n < len; n++) {
        character = texttosvgize[n];
        if (character == '\n') {
            offsetX = 0; 
            var bbox = Raphael.pathBBox(missingCharacterData.d); 
console.log(bbox); 
            fixedTopOffset += (bbox.height+1)*lscale; 
            continue; 
        }
        
        characterData = characters[character];

        if (characterData == null) {
            characterData = missingCharacterData;
        }

        // case of Jackie's segments which have no widths and need to be scaled, justified and not flipped
        if (characterData.w === undefined) {
            var bbox = Raphael.pathBBox(characterData.d)
            scaledOffsetX = offsetX * lscale + fixedLeftOffset;
            scaledOffsetY = fixedTopOffset;

            // Construct a path element with the matrix set to scale and translate as required. (don't put a '+' in front of the scale, it breaks the Raphael's matrix parser)
            path = '<path transform="matrix(' + lscale + ',0,0,-' + lscale +',' + (scaledOffsetX-bbox.x*lscale) + ',' + (scaledOffsetY) + ')" d="' + characterData.d + '" />\n';
            // Move the 'cursor' and add on the letter gap
            offsetX += (character == ' ' ? 3 : bbox.width)+1.0;
            
        } else {
            scaledOffsetX = offsetX * scale + fixedLeftOffset;
            scaledOffsetY = fixedTopOffset;

            // Construct a path element with the matrix set to scale and translate as required.
            path = '<path transform="matrix(' + scale + ', 0, 0, -' + scale +',' + scaledOffsetX + ',' + (scaledOffsetY) + ')" d="' + characterData.d + '" />\n';

            // Move the 'cursor'
            offsetX += characterData.w;
        }
        paths.push(path);
    }

    var res = ['<?xml version="1.0" encoding="UTF-8" standalone="no"?>', 
               '<svg viewBox="0 0 744.09448819 1052.3622047" height="297mm" width="210mm">', 
               '  <g style="fill:none;stroke:#800000">',
               paths.join(""),
               '  </g>',
               '</svg>'];
    result = res.join("\n");

    return result;
}


// attempts to remove the double lines in the text have failed
// need another font that is more regularized, say
/*
ss = [ ]
for si in s.split("\n"):
    m = re.search('d": "(.*?)"', si)
    ss.append([si[:m.start(1)], m.group(1), si[m.end(1):]])

for k1, k, k2 in ss:
    assert k[-1] == "z"
    kz = re.findall("(.*?z)", k)

res = [ ]
for k1, k, k2 in ss:
    assert k[-1] == "z"
    kzs = re.findall("(.*?z)", k)
    zres = [ ]
    for kz in kzs:
        j = re.findall("([^0-9.\- ])([0-9.\- ]*)", kz)
        jc = [ ]
        for i in range(2, len(j) - 1):
            l0 = list(map(float, j[i-1][1].split()))[-2:]
            l1 = list(map(float, j[i][1].split()))[-2:]
            if len(l0) == len(l1):
                jc.append((min(abs(x0+x1)  for x0, x1 in zip(l0, l1)), i))
        ic = len(j)-1
        if jc:
            i = min(jc)[1]
            #print(k1[12], min(jc)[0], i, len(j), j[i-1], j[i])
            if min(jc)[0] <= 4:
                ic = i
        z = "".join(a+b  for a, b in j[:ic])
        zres.append(z)
        #print(k1[12], z)
    res.append(k1+"".join(zres)+k2)
print("\n".join(res))
*/
