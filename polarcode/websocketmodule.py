#!/usr/bin/env python3

import sys, socket, time, logging
import asyncio, websockets

def getipaddress():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]

ipaddress = "localhost"
ipaddress = getipaddress()

mainasyncloop = asyncio.get_event_loop()
mainasyncloop.set_debug(True)

webconnectioncount = 0
allwebsockets = [ ]  # could more correctly use rwebsockets to handle this
asyncqueueoutward = asyncio.Queue()
asyncqueueoutward.put_nowait("jjjj")
newwebsockets = asyncio.Queue()
asyncqueueinward = asyncio.Queue()

posstring = "initial header on connection"
def setstartpositionstring(lposstring):
    global posstring
    posstring = lposstring
    mainasyncloop.call_soon_threadsafe(asyncqueueoutward.put_nowait, posstring)

@asyncio.coroutine
def senderall():
    while True:
        v = yield from asyncqueueoutward.get()
        for ws in list(allwebsockets):
            try:
                yield from ws.send(v)
            except websockets.exceptions.ConnectionClosed:
                print("remm", ws)
                if ws in allwebsockets:
                    allwebsockets.remove(ws)

@asyncio.coroutine
def receiverall():
    pendinglist = [ asyncio.async(newwebsockets.get()) ]
    rwebsockets = [ ]
    T0 = time.time()
    while True:
        T1 = time.time()
        done, pending = yield from asyncio.wait(pendinglist, return_when=asyncio.FIRST_COMPLETED)
        for task in done:
            i = pendinglist.index(task)
            if i == 0:
                ws = task.result()
                rwebsockets.append(ws)
                pendinglist.append(asyncio.async(ws.recv()))
                pendinglist[0] = asyncio.async(newwebsockets.get())
                asyncqueueinward.put_nowait(("newconnection", ws.webconnectionnumber, len(pendinglist)-1, len(rwebsockets)))
            elif task.exception():
                del rwebsockets[i-1]
                del pendinglist[i]
            else:
                #print("mmm", [task.result()])
                #print(time.time() - T0, "rr", task.result())
                asyncqueueinward.put_nowait((task.result(), ws.webconnectionnumber, i-1, len(rwebsockets)))
                pendinglist[i] = asyncio.async(ws.recv())
        assert len(pendinglist) == len(rwebsockets)+1


@asyncio.coroutine
def handler(ws, path):
    global webconnectioncount
    ws.webconnectionnumber = webconnectioncount
    webconnectioncount += 1
    print("connected", ws)
    allwebsockets.append(ws)
    yield from newwebsockets.put(ws)   # necessary to get into the pending list in the receiverall task
    yield from asyncio.wait([ws.send("Hello! #" + str(webconnectioncount))])
    yield from asyncio.wait([ws.send(posstring)])
    try:
        while ws in allwebsockets:
            yield from asyncio.sleep(10)
    finally:
        if ws in allwebsockets:
            allwebsockets.remove(ws)
        print("disconnecting", ws)


def servewebsocketrunforever(portnumber):
    print("serving", ipaddress, portnumber)
    start_server = websockets.serve(handler, ipaddress, portnumber)
    mainasyncloop.run_until_complete(start_server)
    receivertask = asyncio.async(receiverall())
    sendertask = asyncio.async(senderall())
    mainasyncloop.run_forever()

    

